namespace UniversityManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Result",
                c => new
                    {
                        ResultID = c.Int(nullable: false, identity: true),
                        SubjectID = c.Int(nullable: false),
                        StudentID = c.Int(nullable: false),
                        SpecID = c.Int(nullable: false),
                        Course = c.Int(nullable: false),
                        FacultyNumber = c.Int(nullable: false),
                        Grade = c.Int(),
                    })
                .PrimaryKey(t => t.ResultID)
                .ForeignKey("dbo.Student", t => t.StudentID, cascadeDelete: true)
                .ForeignKey("dbo.Subject", t => t.SubjectID, cascadeDelete: true)
                .Index(t => t.SubjectID)
                .Index(t => t.StudentID);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        StudentID = c.Int(nullable: false, identity: true),
                        StudentName = c.String(nullable: false, maxLength: 50),
                        SpecialityID = c.Int(nullable: false),
                        Course = c.Int(nullable: false),
                        FacultyNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudentID)
                .ForeignKey("dbo.Speciality", t => t.SpecialityID, cascadeDelete: true)
                .Index(t => t.SpecialityID);
            
            CreateTable(
                "dbo.Speciality",
                c => new
                    {
                        SpecialityID = c.Int(nullable: false, identity: true),
                        SpecialityName = c.String(nullable: false, maxLength: 50),
                        FacultyName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.SpecialityID);
            
            CreateTable(
                "dbo.Subject",
                c => new
                    {
                        SubjectID = c.Int(nullable: false, identity: true),
                        SpecID = c.Int(nullable: false),
                        SubjectName = c.String(nullable: false, maxLength: 50),
                        Speciality = c.String(nullable: false, maxLength: 50),
                        Credits = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SubjectID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Result", "SubjectID", "dbo.Subject");
            DropForeignKey("dbo.Student", "SpecialityID", "dbo.Speciality");
            DropForeignKey("dbo.Result", "StudentID", "dbo.Student");
            DropIndex("dbo.Student", new[] { "SpecialityID" });
            DropIndex("dbo.Result", new[] { "StudentID" });
            DropIndex("dbo.Result", new[] { "SubjectID" });
            DropTable("dbo.Subject");
            DropTable("dbo.Speciality");
            DropTable("dbo.Student");
            DropTable("dbo.Result");
        }
    }
}
