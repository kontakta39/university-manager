﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManager.Models;

namespace UniversityManager.Controllers
{
    public class ResultController : Controller
    {
        private UniversityContext db = new UniversityContext();

        public ActionResult Search(string SearchBox)
        {
            var results = (from t in db.Results
                            where
                                t.subject.SubjectName.Contains(SearchBox)
                                || t.student.StudentName.Contains(SearchBox)
                                || t.subject.Speciality.Contains(SearchBox)
                           select t).ToList();
            return View("Index", results);
        }

        // GET: Result
        public ActionResult Index()
        {
            return View(db.Results.ToList());
        }

        // GET: Result/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Result result = db.Results.Find(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // GET: Result/Create
        public ActionResult Create()
        {
            ViewBag.SubjectID = new SelectList(db.Subjects, "SubjectID", "SubjectName");
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "StudentName");
            ViewBag.SpecID = new SelectList(db.Subjects, "SpecID", "Speciality");
            return View();
        }

        // POST: Result/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ResultID,StudentID,SubjectID,SpecID,Course,FacultyNumber,Grade")] Result result)
        {
            if (ModelState.IsValid)
            {
                db.Results.Add(result);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SubjectID = new SelectList(db.Subjects, "SubjectID", "SubjectName", result.SubjectID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "StudentName", result.StudentID);
            ViewBag.SpecID = new SelectList(db.Subjects, "SpecID", "Speciality", result.SpecID);
            return View(result);
        }

        // GET: Result/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Result result = db.Results.Find(id);
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.SubjectID = new SelectList(db.Subjects, "SubjectID", "SubjectName", result.SubjectID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "StudentName", result.StudentID);
            ViewBag.SpecID = new SelectList(db.Subjects, "SpecID", "Speciality", result.SpecID);
            return View(result);
        }

        // POST: Result/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ResultID,StudentID,SubjectID,SpecID,Course,FacultyNumber,Grade")] Result result)
        {
            if (ModelState.IsValid)
            {
                db.Entry(result).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SubjectID = new SelectList(db.Subjects, "SubjectID", "SubjectName", result.SubjectID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "StudentName", result.StudentID);
            ViewBag.SpecID = new SelectList(db.Subjects, "SpecID", "Speciality", result.SpecID);
            return View(result);
        }

        // GET: Result/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Result result = db.Results.Find(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Result/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Result result = db.Results.Find(id);
            db.Results.Remove(result);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
