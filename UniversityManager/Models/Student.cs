﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UniversityManager.Models
{
    public class Student
    {
        [Key]
        public int StudentID { get; set; }

        [Required(ErrorMessage = "Student name is required")]
        [MaxLength(50)]
        [Display(Name = "Student")]
        public string StudentName { get; set; }

        //Speciality
        [Required(ErrorMessage = "Speciality name is required")]
        public int SpecialityID { get; set; }

        [Required(ErrorMessage = "Number of course is required")]
        [Range(1, 4, ErrorMessage = "Number of course must be between 1 and 4")]
        public int Course { get; set; }

        [Required(ErrorMessage = "Faculty number is required")]
        [Display(Name = "Faculty Number")]
        public int FacultyNumber { get; set; }

        public virtual Speciality speciality { get; set; }
        public virtual ICollection<Result> Results { get; set; }

    }
}