﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UniversityManager.Models
{
    public class Result
    {
        [Key]
        public int ResultID { get; set; }

        //Subject Name
        [Required(ErrorMessage = "Subject name is required")]
        public int SubjectID { get; set; }

        //Student Name
        [Required(ErrorMessage = "Student name is required")]
        public int StudentID { get; set; }

        //Speciality
        [Required(ErrorMessage = "Speciality name is required")]
        public int SpecID { get; set; }

        [Required(ErrorMessage = "Course name is required")]
        [Range(1, 4, ErrorMessage = "Course number must be between 1 and 4")]
        public int Course { get; set; }

        [Required(ErrorMessage = "Faculty number is required")]
        [Display(Name = "Faculty Number")]
        public int FacultyNumber { get; set; }

        [Range(2, 6, ErrorMessage = "Grade must be between 2 and 6")]
        public int? Grade { get; set; }

        public virtual Student student { get; set; }
        public virtual Subject subject { get; set; }

    }
}