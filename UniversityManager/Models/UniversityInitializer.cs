﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UniversityManager.Models
{
    public class UniversityInitializer :DropCreateDatabaseIfModelChanges<UniversityContext>
    {
        protected override void Seed(UniversityContext context)
        {
            var specialities = new List<Speciality>
            {
                new Speciality {SpecialityName = "Computer Science", FacultyName = "Mathematics and Informatics"},
                new Speciality {SpecialityName = "Software Engineering", FacultyName = "Mathematics and Informatics"}
            };
            foreach (var temp in specialities)
            {
                context.Specialities.Add(temp);
            }
            context.SaveChanges();

            var students = new List<Student>
            {
                new Student {StudentName = "Dimitar Georgiev", SpecialityID = 1, Course = 2, FacultyNumber = 13532},
                new Student {StudentName = "Nikolay Simeonov", SpecialityID = 2, Course = 3, FacultyNumber = 10486}
            };
            foreach (var temp in students)
            {
                context.Students.Add(temp);
            }
            context.SaveChanges();

            var subjects = new List<Subject>
            {
                new Subject {SubjectName = "Java", Speciality = "Computer Science", Credits = 4},
                new Subject {SubjectName = "C#", Speciality = "Software Engineering", Credits = 5}
            };
            foreach (var temp in subjects)
            {
                context.Subjects.Add(temp);
            }
            context.SaveChanges();

            var results = new List<Result>
            {
                new Result {SubjectID = 1, StudentID = 1, SpecID = 1, Course = 2, FacultyNumber = 13532, Grade = 5},
                new Result {SubjectID = 2, StudentID = 2, SpecID = 2, Course = 3, FacultyNumber = 10486, Grade = 4}
            };
            foreach (var temp in results)
            {
                context.Results.Add(temp);
            }
            context.SaveChanges();

        }
    }
}