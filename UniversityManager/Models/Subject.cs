﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace UniversityManager.Models
{
    public class Subject
    {
        [Key]
        public int SubjectID { get; set; }

        public int SpecID { get; set; }

        [Required(ErrorMessage = "Subject name is required")]
        [MaxLength(50)]
        [Display(Name = "Subject")]
        public string SubjectName { get; set; }

        [Required(ErrorMessage = "Speciality name is required")]
        [MaxLength(50)]
        public string Speciality { get; set; }

        [Required(ErrorMessage = "Number of credits is required")]
        [Range(1,6, ErrorMessage = "Number of credits must be between 1 and 6")]
        public int Credits { get; set; }

        public ICollection<Result> Results { get; set; }

    }
}