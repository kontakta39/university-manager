﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace UniversityManager.Models
{
    public class Speciality
    {
        [Key]
        public int SpecialityID { get; set; }

        [Required(ErrorMessage = "Speciality Name is required")]
        [MaxLength(50)]
        [Display(Name = "Speciality")]
        public string SpecialityName { get; set; }

        [Required(ErrorMessage = "Faculty Name is required")]
        [MaxLength(50)]
        [Display(Name = "Faculty")]
        public string FacultyName { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}